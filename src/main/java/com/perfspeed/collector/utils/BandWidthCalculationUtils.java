package com.perfspeed.collector.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class BandWidthCalculationUtils {

	  // Bytes constants
	  public static final double SPACE_KB = 1024;
	  public static final double SPACE_MB = 1024 * SPACE_KB;
	  public static final double SPACE_GB = 1024 * SPACE_MB;
	  public static final double SPACE_TB = 1024 * SPACE_GB;
	  
	  
	  // Mapping of Kbytes
	  public static final String  TYPE1 = "0-64Kbps";
	  public static final String  TYPE2 = "64kbps-512kbps";
	  public static final String  TYPE3 = "512kbps-2mbps";
	  public static final String  TYPE4 = "2mbps-6mbps";
	  public static final String  TYPE5 = "6mbps-10mbps";
	  public static final String  TYPE6 = "10mbps-100mbps";

	  public static String  kbytesToString(final long sizeInKBytes){
		  final NumberFormat nf = new DecimalFormat();
		  nf.setMaximumFractionDigits(0);
		  try {
		     if ( sizeInKBytes < SPACE_MB ) {
		        return nf.format(sizeInKBytes/SPACE_KB) + " kbps";
		      } else if ( sizeInKBytes < SPACE_GB ) {
		        return nf.format(sizeInKBytes/SPACE_MB) + " mbps";
		      } else if ( sizeInKBytes < SPACE_TB ) {
		        return nf.format(sizeInKBytes/SPACE_GB) + " gbps";
		      } else {
		        return nf.format(sizeInKBytes/SPACE_TB) + " tbps";
		      }
		    } catch (Exception e) {
		      return sizeInKBytes + " KBytes(s)";
		    }
	  }
	  
	  public static String mapSizeToType(final String sizeString){
		  final String[] sizeArr = sizeString.split(" ");
		  final int size = Integer.parseInt(sizeArr[0]);
		  if(sizeArr[1].endsWith("kbps") && size <= 64)
			  return TYPE1;
		  if(sizeArr[1].endsWith("kbps") && size <= 512)
			  return TYPE2;
		  if( (sizeArr[1].endsWith("kbps") && size > 512 ) || (sizeArr[1].endsWith("mbps") & size <= 2 ) )
			  return TYPE3;
		  if( (sizeArr[1].endsWith("mbps") && size > 2) && (sizeArr[1].endsWith("mbps") && size <= 6 )  )
			  return TYPE4;
		  if( (sizeArr[1].endsWith("mbps") && size > 6) && (sizeArr[1].endsWith("mbps") && size <= 10) )
			  return TYPE5;
		  if( sizeArr[1].endsWith("mbps") && size > 10 )
			  return TYPE6;
		  return TYPE1; // default return type1
	  }
	  
	  public static void main(String[] args) {
		  long size = 172482;
		System.out.println(kbytesToString(size));
		System.out.println(mapSizeToType(kbytesToString(size)));
	}
}
