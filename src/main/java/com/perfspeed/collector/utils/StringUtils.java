package com.perfspeed.collector.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class StringUtils {

	
	public static Map<String, Object> parseMap(final String input) {
        final Map<String, Object> map = new HashMap<String, Object>();
        for (final String pair : input.split("&")) {
        	if( Objects.nonNull(pair) && !pair.isEmpty() ){
        		 final String[] kv = pair.split("=");
                 if( Objects.nonNull(kv) && kv.length > 1  && Objects.nonNull(kv[0]) && !kv[0].isEmpty() 
                		 && Objects.nonNull(kv[1]) && !kv[1].isEmpty())
                 		map.put(kv[0].toLowerCase(), kv[1].toLowerCase());
        	}
        }
        return map;
    }
	
	public static boolean isNullOrEmpty(final String input ){
		return Objects.isNull(input) || input.isEmpty();
	}
	
	public static String deCode(String url) {
		try {
			return URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return "failed to decode url " + e.getMessage();
		}
	}
	
	public static void deCodeMapData(final Map<String,Object> dataMap){
		for(final String key : dataMap.keySet()){
			String decodedData = null;
			try{
				decodedData = deCode(dataMap.get(key).toString());
			}catch (Exception e) {
				e.printStackTrace();
			}
			if(Objects.nonNull(decodedData) && !decodedData.isEmpty())
				dataMap.put(key, decodedData);
		}
	}
	
	/**
	 * Convert Base36 String to Integer 
	 * @param base36
	 * @return
	 */
	public static int decodeBase36StrToInt(final String base36){
		try{
			return new BigInteger(base36, 36).intValue();
		}catch (Exception e) {
		}
		return 0;
	}
}
