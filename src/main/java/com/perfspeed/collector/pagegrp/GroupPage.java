package com.perfspeed.collector.pagegrp;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.perfspeed.collector.utils.CollectorUtils;

public class GroupPage {

	private static final Logger LOG = LoggerFactory.getLogger(GroupPage.class);
	
	private static final String DEFAULT_PG_GRP = "home";
	private static final String OTHERS_PG_GRP = "others";
	private static final String PATH_SPLITTER = "/";
	
	public static void addPageGroup(final Map<String,Object> dataMap) throws Exception{
		Object urlObj = dataMap.get("nu");
		if(Objects.isNull(urlObj)) urlObj = dataMap.get("u");
		if(Objects.nonNull(urlObj) && !urlObj.toString().isEmpty()){
			final URL url = new URL(urlObj.toString());
			final String tenantId = CollectorUtils.getTenantId(url);
			dataMap.put("tenant_id", tenantId);
			if(Objects.isNull(url)) return;
			dataMap.put("page_group", findPageGroup(url));
		}
	}

	/**
	 * Determine page group using url
	 * @param url
	 * @return 
	 * @throws MalformedURLException
	 */
	private static String findPageGroup(final URL url) throws MalformedURLException{
		try{
			final String urlPath = url.getPath();
			if( Objects.isNull(urlPath)  || urlPath.isEmpty())
				return DEFAULT_PG_GRP;
			// Example urlpath /home/data
			final String[] paths = urlPath.replaceFirst(PATH_SPLITTER, "").split(PATH_SPLITTER);
			if(paths.length == 0 || paths.length < 1 ) return OTHERS_PG_GRP; // dont know what page :)
			return paths[0]; // first index
		}catch (Exception e) {
			LOG.error("Failed to determine page group for url : {} , error : ", url , e.getMessage());
		}
		return OTHERS_PG_GRP;
	}
	
	public static void main(String[] args) throws Exception {
		String urlTxt = "http://www.decorpot.com/connect";
		final URL url = new URL(urlTxt);
		System.out.println(findPageGroup(url));
	}
}