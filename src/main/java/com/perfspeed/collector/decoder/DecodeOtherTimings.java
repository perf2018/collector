package com.perfspeed.collector.decoder;

import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class DecodeOtherTimings {

	public static void decodeOther(final Map<String,Object> dataMap){
		final Object tOther = dataMap.get("t_other");
		if(Objects.isNull(tOther) || tOther.toString().isEmpty()) return;
		final String[] tmpArr = tOther.toString().split(",");
		for(final String data : tmpArr ){
			if(Objects.nonNull(data) && !data.isEmpty() ){
				final String[] dataArr = data.split(Pattern.quote("|"));
				dataMap.put(dataArr[0].trim(), dataArr[1].trim());
			}
		}
	}
}
