package com.perfspeed.collector.decoder;

import java.math.BigInteger;
import java.util.Objects;

import com.perfspeed.collector.utils.StringUtils;

public class Test {

	public static void main(String[] args) {
		String base36 = "1";
		System.out.println( new BigInteger(base36, 36).intValue());
		
		String data = "5bup,gz,gx,3*1,56".substring(1);
		final String[] tmp = data.split(",");
		final int[] timings = new int[tmp.length];
		for (int i = 0; i < tmp.length ; i++ ){
			if(Objects.isNull(tmp[i])) timings[i] = 0;
			else	timings[i] = StringUtils.decodeBase36StrToInt(tmp[i]);
		}
		
		final int startTime = timings[0];
		for(final String field : RTDecodeRerReferenceData.FIELDS){
			int index = RTDecodeRerReferenceData.TIMEING_INDEX_MAPPING.get(field);
			int beforeTime = timings[index];
			int time = findResourceTime(timings, index, startTime);
			System.out.println(field +" : "+beforeTime +"  : "+time + " : "+ startTime);
		}
	}
	
	public static int findResourceTime(final int[] timings , final int index , final int startTime ){
		int result = 0;
		boolean isIndexExists = indexExists(index, timings);
		if(isIndexExists){
			result = timings[index] + startTime;
		}
		return result;
	}
	
	public static boolean indexExists(final int index , final int[] timeArr){
		return index >= 0 && index < timeArr.length;
	}
}
