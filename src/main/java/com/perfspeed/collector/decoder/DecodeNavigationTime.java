package com.perfspeed.collector.decoder;

import java.util.Map;
import java.util.Objects;

public class DecodeNavigationTime {

	public static void decodeNT(final Map<String,Object> dataMap){
		// DNS Time
		final Object dnsLookUpEnd = dataMap.get("nt_dns_end");
		final Object dnsLookUpSt = dataMap.get("nt_dns_st");
		if( Objects.nonNull(dnsLookUpEnd) && Objects.nonNull(dnsLookUpSt)){
			final long endTime = Long.valueOf(dnsLookUpEnd.toString());
			final long startTime = Long.valueOf(dnsLookUpSt.toString());
			dataMap.put("dns", (endTime - startTime));
		}
		
		// TCP Time
		final Object ntConnEnd = dataMap.get("nt_con_end");
		final Object ntConnSt = dataMap.get("nt_con_st");
		if( Objects.nonNull(ntConnEnd) && Objects.nonNull(ntConnSt)){
			final long endTime = Long.valueOf(ntConnEnd.toString());
			final long startTime = Long.valueOf(ntConnSt.toString());
			dataMap.put("tcp", (endTime - startTime));
		}
		
		// Dom Loading Time
		final Object domLoadEnd = dataMap.get("nt_domcontloaded_end");
		final Object domLoadSt = dataMap.get("nt_domcontloaded_st");
		if( Objects.nonNull(domLoadEnd) && Objects.nonNull(domLoadSt)){
			final long endTime = Long.valueOf(ntConnEnd.toString());
			final long startTime = Long.valueOf(ntConnSt.toString());
			dataMap.put("dom", (endTime - startTime));
		}
		
		// Time to First Byte
		final Object respSt = dataMap.get("nt_res_st");
		final Object reqSt = dataMap.get("nt_req_st");
		if( Objects.nonNull(respSt) && Objects.nonNull(reqSt)){
			final long endTime = Long.valueOf(respSt.toString());
			final long startTime = Long.valueOf(reqSt.toString());
			dataMap.put("ttfb", (endTime - startTime));
		}
		
		// Network Latency
		final Object respEnd = dataMap.get("nt_res_end");
		if( Objects.nonNull(respEnd) && Objects.nonNull(reqSt)){
			final long endTime = Long.valueOf(respEnd.toString());
			final long startTime = Long.valueOf(reqSt.toString());
			dataMap.put("latency", (endTime - startTime));
		}
		
		// DownLoad Time
		if( Objects.nonNull(respSt) && Objects.nonNull(respEnd)){
			final long endTime = Long.valueOf(respEnd.toString());
			final long startTime = Long.valueOf(respSt.toString());
			dataMap.put("download", (endTime - startTime));
		}
		
		// SSL Time
		final Object sslSt = dataMap.get("nt_ssl_st");
		if( Objects.nonNull(sslSt) && Objects.nonNull(ntConnEnd)){
			final long endTime = Long.valueOf(ntConnEnd.toString());
			final long startTime = Long.valueOf(sslSt.toString());
			dataMap.put("ssl", (endTime - startTime));
		}
	}
	
	public static void deCodeBOOMRTimings(final Map<String,Object> dataMap){
		// pageload time
		final Object pageLoadTime = dataMap.get("t_done");
		if(Objects.nonNull(pageLoadTime) && !pageLoadTime.toString().isEmpty()){
			Long time = Long.valueOf(pageLoadTime.toString());
			dataMap.put("t_done", time);
		}
		final Object backEndTime = dataMap.get("t_resp");
		if(Objects.nonNull(backEndTime) && !backEndTime.toString().isEmpty()){
			Long time = Long.valueOf(backEndTime.toString());
			dataMap.put("t_resp", time);
		}
		final Object frontEndTime = dataMap.get("t_page");
		if(Objects.nonNull(frontEndTime) && !frontEndTime.toString().isEmpty()){
			Long time = Long.valueOf(frontEndTime.toString());
			dataMap.put("t_page", time);
		}
	}
}
