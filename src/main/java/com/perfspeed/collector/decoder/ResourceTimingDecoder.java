package com.perfspeed.collector.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.perfspeed.collector.utils.JsonParser;
import com.perfspeed.collector.utils.StringUtils;

public class ResourceTimingDecoder {

	private static final Logger LOG = LoggerFactory.getLogger(ResourceTimingDecoder.class);

	// for special data 
	private static final String SPECIAL_DATA_PREFIX = "*";
	private static final String SPECIAL_DATA_DIMENSION_TYPE = "0";
	private static final String SPECIAL_DATA_DIMENSION_PREFIX = SPECIAL_DATA_PREFIX + SPECIAL_DATA_DIMENSION_TYPE;
	private static final String SPECIAL_DATA_SIZE_TYPE = "1";
	private static final String SPECIAL_DATA_SCRIPT_TYPE = "2";
	private static final String SPECIAL_DATA_SERVERTIMING_TYPE = "3";
	
	
	public static String deCompressRT(final String rt ){
		if(StringUtils.isNullOrEmpty(rt)){
			LOG.debug("Resource Time is Empty or Null : {} ", rt);
			return null;
		}
		final String deCodedStr = StringUtils.deCode(rt);
		final Map<String, Object> resourceAndTimings = new HashMap<>();
		processChildNode(deCodedStr, "" , resourceAndTimings); 
		final List<Map<String,Object>> resourceTimings = new ArrayList<>();
		for(final String URL : resourceAndTimings.keySet())
		resourceTimings.addAll(decodeRT(URL, resourceAndTimings.get(URL).toString()));
		return JsonParser.objectToJson(resourceTimings);
	}
	
	/**
	 * Convert Plain Raw data into Json and Verify whether it has Child Node or Not  - Recursive API
	 * @param data
	 * @param finalURL
	 * @param resourceAndTimings
	 * @return true or false and fills up the Map with Key as Complete URL and Value as time/special/size data 
	 */
	public static boolean processChildNode(final String data, String finalURL , final Map<String, Object> resourceAndTimings){
		final JSONObject childJson = strToJson(data);
		if(Objects.isNull(childJson)) return false; 
		final Set<String> keys = childJson.keySet();
		for(final String key : keys){
			final String value = childJson.get(key).toString();
			String newURL  = finalURL+key;
			if(!processChildNode(value, newURL, resourceAndTimings))
				resourceAndTimings.put(newURL.trim(), value);
		}
		return true;
	}

	/**
	 * Decodes Each URL Resource Timing data 
	 * @param name
	 * @param rtValues
	 * @return list Of Resources with Clean JSON data 
	 */
	public static List<Map<String,Object>> decodeRT( final String name , final String rtValues ){
		final List<Map<String,Object>> rtList = new ArrayList<>();
		try{
			// if value starts with *0 its a Special Data 
			// figure out if it is special data or not 
			if(rtValues.startsWith(SPECIAL_DATA_PREFIX)){
				final Map<String,Object> rtInfoMap = new HashMap<>();
				final String[] valueArr = rtValues.split(Pattern.quote("|"));
				final String specialData = valueArr[0];
				final String timeData = valueArr[1];
				rtInfoMap.put("name", name);
				decompressTimeStamps(timeData, rtInfoMap);
				decompressSpecialData(specialData, rtInfoMap);
				rtList.add(rtInfoMap);
			}else{
				final String[] valueArr = rtValues.split(Pattern.quote("|"));
				for(final String value : valueArr ){
					final Map<String,Object> rtInfoMap = new HashMap<>();
					rtInfoMap.put("name", name);
					final String[] data = value.split(Pattern.quote(SPECIAL_DATA_PREFIX));
					String sizeData = null;
					if(data.length > 1 ) sizeData = data[1];
					decompressTimeStamps(data[0], rtInfoMap);
					decompressSpecialData(sizeData, rtInfoMap);
					rtList.add(rtInfoMap);
				}
			}
		}catch (Exception e) {
			LOG.error("Failed to decode Resouce Time ", e);
		}
		return rtList;
	}
	
	/**
	 * Decompress the SIZE data to figure out transferSize , bodySize
	 * @param sizeData
	 * @param rtInfoMap
	 */
	private static void decompressSize(final String sizeData , final Map<String,Object> rtInfoMap){
		if(Objects.isNull(sizeData) || sizeData.isEmpty() ) return;
		final String[] tmpArr = sizeData.split(",");
		final int[] sizeArr = new int[20];
		for(int i = 0 ; i < tmpArr.length ; i++ ){
			if(tmpArr[i].equalsIgnoreCase("_")) sizeArr[i] = 0;
			else{
				if (tmpArr[i].equalsIgnoreCase("")) sizeArr[i] = 0 ;
				int value = StringUtils.decodeBase36StrToInt(tmpArr[i]);
				sizeArr[i] = value;
				if(i > 0) sizeArr[i] += sizeArr[0];
			}
		}
		// fill in missing
		if(tmpArr.length == 1 ) sizeArr[1] = sizeArr[0];
		if (tmpArr.length == 2) sizeArr[2] = sizeArr[0];
		rtInfoMap.put("encodedBodySize", sizeArr[0]);
		rtInfoMap.put("transferSize", sizeArr[1]);
		rtInfoMap.put("decodedBodySize", sizeArr[2]);
	}
	
	/**
	 * Decompress all timestamps ( start time , end time etc ...)
	 * @param timingsData
	 * @param rtInfoMap
	 */
	private static void decompressTimeStamps(final String timingsData, final Map<String,Object> rtInfoMap){
		final String[] timeArr = timingsData.split(",");
		final String startTime = timeArr[0];
		if(startTime.length() <= 1 ) return;
		final String initiatorType = startTime.substring(0, 1);
		timeArr[0] = startTime.substring(1);
		int navigationStartTime = StringUtils.decodeBase36StrToInt(timeArr[0]);
		rtInfoMap.put("startTime", navigationStartTime);
		rtInfoMap.put("fetchStart",navigationStartTime);
		rtInfoMap.put("initiatorType", getInitiatorType(initiatorType));
		rtInfoMap.put("duration", 0);
		for(final String field : RTDecodeRerReferenceData.FIELDS){
			int index = RTDecodeRerReferenceData.TIMEING_INDEX_MAPPING.get(field);
			if(indexExists(index, timeArr)){
				int time = StringUtils.decodeBase36StrToInt(timeArr[index]);
				int finalTime = time+navigationStartTime;
				rtInfoMap.put(field, finalTime);
				// add fetchStart
				if(index == 9 ) rtInfoMap.put("fetchStart", finalTime);
			}
		}
		// add duration
		if(rtInfoMap.containsKey("responseEnd") && Objects.nonNull(rtInfoMap.get("responseEnd"))){
			int duration = ( Integer.valueOf(rtInfoMap.get("responseEnd").toString()) - Integer.valueOf(rtInfoMap.get("startTime").toString())) ;
			rtInfoMap.put("duration", duration);
		}
	}

	/**
	 * Decompress the dimension data x,y, height, width
	 * @param resourceData
	 * @param rtInfoMap
	 */
	private static void decompressDimensions(final String resourceData, final Map<String,Object> rtInfoMap){
		if(!isDimensionData(resourceData)) return;
		final String[] dimensionDataArr = resourceData.substring(SPECIAL_DATA_DIMENSION_PREFIX.length()).split(",");
		if(dimensionDataArr.length < 2 ) return; // atleast height and width should be there 
		 for (int i = 0; i < dimensionDataArr.length; i++) {
			 if (dimensionDataArr[i].isEmpty()) {
				 	rtInfoMap.put(RTDecodeRerReferenceData.DIMENSION_NAMES.get(i), 0);
	            } else {
	                int dimensionVal = StringUtils.decodeBase36StrToInt(dimensionDataArr[i]);
	                rtInfoMap.put(RTDecodeRerReferenceData.DIMENSION_NAMES.get(String.valueOf(i)), dimensionVal);
	            }
		 }
		// If naturalHeight and naturalWidth are missing, then they are the same as height and width
		 if(!rtInfoMap.containsKey("naturalHeight"))
			 rtInfoMap.put("naturalHeight", rtInfoMap.get("height"));
		 if(!rtInfoMap.containsKey("naturalWidth"))
			 rtInfoMap.put("naturalWidth", rtInfoMap.get("width"));
	}
	
	/**
	 * Special Data Handling 
	 * @param resourceData
	 * @param rtInfoMap
	 */
	private static void decompressSpecialData(String resourceData, final Map<String,Object> rtInfoMap){
		if(Objects.isNull(resourceData) || resourceData.isEmpty() ) return;
		final String dataType = resourceData.substring(0,1);
		if(SPECIAL_DATA_SIZE_TYPE.equalsIgnoreCase(dataType)){
			resourceData = resourceData.substring(1);
			decompressSize(resourceData, rtInfoMap);
		}else if(isDimensionData(resourceData)){
			decompressDimensions(resourceData, rtInfoMap);
		}else if(SPECIAL_DATA_SCRIPT_TYPE.equalsIgnoreCase(dataType)){
			// add API
		}else if(SPECIAL_DATA_SERVERTIMING_TYPE.equalsIgnoreCase(dataType)){
			// add API
		}
	}
	
	/**
	 * Verify if the given data belongs to dimension or not 
	 * @param resourceData
	 * @return
	 */
	private static boolean isDimensionData(final String resourceData ){
		return Objects.nonNull(resourceData) && 
				( resourceData.substring(0 , SPECIAL_DATA_DIMENSION_PREFIX.length()).equalsIgnoreCase(SPECIAL_DATA_DIMENSION_PREFIX) );
	}
	
	/**
	 * Check the index exists in Array 
	 * @param index
	 * @param timeArr
	 * @return
	 */
	private static boolean indexExists(final int index , final String[] timeArr){
		return index >= 0 && index < timeArr.length;
	}
	
	/**
	 * Fetch Initiator Name fromm Reference Data 
	 * @param key
	 * @return
	 */
	private static String getInitiatorType(final String key ){
		return RTDecodeRerReferenceData.INITIATOR_TYPES.get(key);
	}
	
	
	/**
	 * Convert String to Json 
	 * @param json
	 * @return
	 */
	private static JSONObject strToJson(final String json){
		try{
			return new JSONObject(json);
		}catch (Exception e) {
		}
		return null;
	}
}
