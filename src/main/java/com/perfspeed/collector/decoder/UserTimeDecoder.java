package com.perfspeed.collector.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import com.perfspeed.collector.utils.JsonParser;
import com.perfspeed.collector.utils.StringUtils;

public class UserTimeDecoder {

	public static final Map<Integer,String> INITIATOR_TYPES = new HashMap<>();
	
	static{
		INITIATOR_TYPES.put(1, "click");
		INITIATOR_TYPES.put(2, "mouse");
		INITIATOR_TYPES.put(3, "key");
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public static String deCodeCT(final String  input ){
		if(Objects.isNull(input) || input.isEmpty() ) return null;
		final List<Map<String,String>> userTimings = new ArrayList<>();
		final String[] ctArr = input.split(Pattern.quote("|"));
		for(final String data : ctArr)
			userTimings.add(parseData(data));
		if(!userTimings.isEmpty())
			return JsonParser.objectToJson(userTimings);
		return null;
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	public static Map<String,String> parseData(final String data ){
		final Map<String,String> result = new HashMap<>();
		final String[] tmpArr = data.split(",");
		if(tmpArr.length <= 0 ) return result;
		final char type = tmpArr[0].charAt(0);
		result.put("type", INITIATOR_TYPES.get(Character.getNumericValue(type)));
		for(String m : tmpArr ){
			final Character typeOfData = m.charAt(0);
			m = m.substring(1);
			if(typeOfData.equals('x')){
				result.put("x", String.valueOf(StringUtils.decodeBase36StrToInt(m)));
			}else if(typeOfData.equals('y')){
				result.put("y", String.valueOf(StringUtils.decodeBase36StrToInt(m)));
			}else{
				result.put("time", String.valueOf(StringUtils.decodeBase36StrToInt(m)));
			}
		}
		return result;
	}
}
