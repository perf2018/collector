package com.perfspeed.collector.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.perfspeed.collector.utils.JsonParser;
import com.perfspeed.collector.utils.StringUtils;

public class TestDecodeApi {

	// remove below 
		public static void  main(String[] args )  {
		  final String input = "%7B%22http%3A%2F%2Fec2-52-90-173-227.compute-1.amazonaws.com%3A9080%2F%22%3A%7B%22users%2Fmanishk%2Fnotifications%22%3A%2255ot%2Ciu%2Cii%2C4%2A1c1%2C8w%2Cst%22%2C%22favicon.ico%22%3A%2205pt%2Cg%2C9%2C5%2A1gq%2C_%2C9k%22%7D%7D";
		  final String decoded = StringUtils.deCode(input);
		  int time = StringUtils.decodeBase36StrToInt("k9");
		  System.out.println(time);
//		  final String decoded = args[0];
		  final Map<String, Object> resourceAndTimings = new HashMap<>();
		  ResourceTimingDecoder.processChildNode(decoded, "" , resourceAndTimings);
		  final List<Map<String,Object>> resourceTimings = new ArrayList<>();
		  for(final String URL : resourceAndTimings.keySet()){
			  System.out.println(URL+" : "+ resourceAndTimings.get(URL));
			  resourceTimings.addAll(ResourceTimingDecoder.decodeRT(URL, resourceAndTimings.get(URL).toString()));
		  }
		  for(Map<String,Object> map : resourceTimings){
			  final String finalJson = JsonParser.mapObj2Json(map);
			  System.out.println(finalJson);
		  }
		  System.out.println(JsonParser.objectToJson(resourceTimings));
		}
}
