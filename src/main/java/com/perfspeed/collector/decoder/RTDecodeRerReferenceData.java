package com.perfspeed.collector.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RTDecodeRerReferenceData {

	public static final Map<String,String> INITIATOR_TYPES = new HashMap<>();
	public static final Map<String,String> DIMENSION_NAMES = new HashMap<>();
	public static final Map<String,String> SCRIPT_ATTRIBUTES = new HashMap<>();
	public static final Map<String,Integer> TIMEING_INDEX_MAPPING = new HashMap<>();
	public static final List<String> FIELDS = new ArrayList<>();
	
	
	static{
		// initiator types
		INITIATOR_TYPES.put("0", "other");
		INITIATOR_TYPES.put("1", "img");
		INITIATOR_TYPES.put("2", "link");
		INITIATOR_TYPES.put("3", "script");
		INITIATOR_TYPES.put("4", "css");
		INITIATOR_TYPES.put("5", "xmlhttprequest");
		INITIATOR_TYPES.put("6", "html");
		INITIATOR_TYPES.put("7", "image");
		INITIATOR_TYPES.put("8", "beacon");
		INITIATOR_TYPES.put("9", "fetch");
		
		DIMENSION_NAMES.put("0", "height");
		DIMENSION_NAMES.put("1", "width");
		DIMENSION_NAMES.put("2", "y");
		DIMENSION_NAMES.put("3", "x");
		DIMENSION_NAMES.put("4", "naturalHeight");
		DIMENSION_NAMES.put("5", "naturalWidth");
		
		SCRIPT_ATTRIBUTES.put("1", "scriptAsync");
		SCRIPT_ATTRIBUTES.put("2", "scriptDefer");
		SCRIPT_ATTRIBUTES.put("4", "scriptBody");
		
		TIMEING_INDEX_MAPPING.put("responseEnd", 1);
		TIMEING_INDEX_MAPPING.put("responseStart", 2);
		TIMEING_INDEX_MAPPING.put("requestStart", 3);
		TIMEING_INDEX_MAPPING.put("connectEnd", 4);
		TIMEING_INDEX_MAPPING.put("secureConnectionStart", 5);
		TIMEING_INDEX_MAPPING.put("connectStart", 6);
		TIMEING_INDEX_MAPPING.put("domainLookupEnd", 7);
		TIMEING_INDEX_MAPPING.put("domainLookupStart", 8);
		TIMEING_INDEX_MAPPING.put("redirectEnd", 9);
		TIMEING_INDEX_MAPPING.put("redirectStart", 10);
		
		FIELDS.add("responseEnd");
		FIELDS.add("responseStart");
		FIELDS.add("requestStart");
		FIELDS.add("connectEnd");
		FIELDS.add("secureConnectionStart");
		FIELDS.add("connectStart");
		FIELDS.add("domainLookupEnd");
		FIELDS.add("domainLookupStart");
		FIELDS.add("redirectEnd");
		FIELDS.add("redirectStart");
		
	}
}
