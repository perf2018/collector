package com.perfspeed.collector.decoder;

import java.util.Map;
import java.util.Objects;

import com.perfspeed.collector.utils.CollectorUtils;

public class DecodeSessionTime {

	public static void decodeSession(final Map<String,Object> dataMap){
		
		final Object sessionEndTime = dataMap.get("session_end_time");
		if(Objects.nonNull(sessionEndTime) &&  !sessionEndTime.toString().isEmpty()){
			dataMap.put("session_end_time", Long.valueOf(sessionEndTime.toString()));
		}
		final Object sessionStartTime = dataMap.get("session_start_time");
		final Object sessionInFocus = dataMap.get("session_in_focus");
		if(Objects.nonNull(sessionStartTime) &&  !sessionStartTime.toString().isEmpty())
			dataMap.put("session_start_time", Long.valueOf(sessionStartTime.toString()));
		if(Objects.nonNull(sessionInFocus) &&  !sessionInFocus.toString().isEmpty())
			dataMap.put("session_in_focus", Long.valueOf(sessionInFocus.toString()));
		
		final Object isUnload = dataMap.get("is_unload");
		if(Objects.isNull(isUnload) || isUnload.toString().isEmpty()) // ensure to always set false in case of no data - we filter this based on this flag
			dataMap.put("is_unload", false);
		// fill tenant id
		final Object domain = dataMap.get("domain");
		if(Objects.nonNull(domain) && !domain.toString().isEmpty()){
			final String tenantId = CollectorUtils.getTenantId(domain.toString());
			dataMap.put("tenant_id", tenantId);
		}
		final Object sessionTotal = dataMap.get("session_total_time");
		if(Objects.nonNull(sessionTotal) &&  !sessionTotal.toString().isEmpty()){
			Long time = Long.valueOf(sessionTotal.toString());
			dataMap.put("session_total_time", time);
		}
	}
}
