package com.perfspeed.collector.decoder;

import java.util.Map;
import java.util.Objects;

import com.perfspeed.collector.utils.BandWidthCalculationUtils;


public class DecodeBW {

	public static void decode(final Map<String,Object> dataMap){
		try{
			final Object bw = dataMap.get("bw");
			if(Objects.nonNull(bw) && !bw.toString().isEmpty()){
				long size = 0;
				if(!"NaN".equalsIgnoreCase(bw.toString())) 
					size = Long.valueOf(bw.toString());
				final String type = BandWidthCalculationUtils.mapSizeToType(BandWidthCalculationUtils.kbytesToString(size));
				dataMap.put("bw_type", type);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
